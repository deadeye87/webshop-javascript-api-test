const superagent = require("superagent");
const assert = require("assert");

// Get-request posts

superagent
    .get('http://localhost:3000/posts')
    .expect(200)
    .expect('Content-Type', 'application/json')
    .expect(function(res) {
        assert(res.body.hasOwnProperty('id'));
        assert(res.body.hasOwnProperty('title'));
        assert(res.body.hasOwnProperty('author'));
    })
    .end(function(err, res) {
        if (err) throw err;
        console.log(res.body);
    });

// Get-request comments

superagent
    .get('http://localhost:3000/comments')
    .expect(200)
    .expect('Content-Type', 'application/json')
    .expect(function(res) {
        assert(res.body.hasOwnProperty('body'));
        assert(res.body.hasOwnProperty('id'));
        assert(res.body.hasOwnProperty('postId'))
    })
    .end(function(err, res) {
        if (err) throw err;
        console.log(res.body);
    });

// Get-request profile

superagent
    .get('http://localhost:3000/profile')
    .expect(200)
    .expect('Content-Type', 'application/json')
    .expect(function(res) {
        assert(res.body.hasOwnProperty('name'));
    })
    .end(function(err, res) {
        if (err) throw err;
        console.log(res.body);
    });

//post request profile

superagent
    .post('http://localhost:3000/profile')
    .set('Content-Type', 'application/json')
    .send("name: Tester")
    .expect(201)
    .expect('Content-Type', 'application/json')
    .expect(function(res) {
        assert(res.body.hasOwnProperty('name'));
    })
    .then(callback)
    .catch(errorCallback)

//post request comments

superagent
    .post('http://localhost:3000/comments')
    .set('Content-Type', 'application/json')
    .send('body: some other comment')
    .expect(201)
    .expect('Content-Type', 'application/json')
    .expect(function(res) {
        assert(res.body.hasOwnProperty('body'));
        assert(res.body.hasOwnProperty('id'));
        assert(res.body.hasOwnProperty('postId'))
    })
    .then(callback)
    .catch(errorCallback)

//post request posts

//put request posts

superagent
    .post('http://localhost:3000/posts')
    .set('Content-Type', 'application/json')
    .send('title: test-json-server', 'author: Tester')
    .expect(201)
    .expect('Content-Type', 'application/json')
    .expect(function(res) {
        assert(res.body.hasOwnProperty('id'));
        assert(res.body.hasOwnProperty('title'));
        assert(res.body.hasOwnProperty('author'));
    })
    .then(callback)
    .catch(errorCallback)

superagent
    .put('http://localhost:3000/posts/1')
    .set('Content-Type', 'application/json')
    .send('title: test-put-json-server', 'author: Tester-put')
    .expect(200)
    .expect('Content-Type', 'application/json')
    .then(callback)
    .catch(errorCallback)

//put request profile

superagent
    .put('http://localhost:3000/profile/1')
    .set('Content-Type', 'application/json')
    .send("name: Tester-put")
    .expect(200)
    .expect('Content-Type', 'application/json')
    .then(callback)
    .catch(errorCallback)

//put request comments

superagent
    .put('http://localhost:3000/comments/1')
    .set('Content-Type', 'application/json')
    .send("body: some other comments to test put ")
    .expect(200)
    .expect('Content-Type', 'application/json')
    .then(callback)
    .catch(errorCallback)

// delete request posts

superagent
    .delete("http://localhost:3000/posts/1")
    .expect(204)
    .then(callback)
    .catch(errorCallback)

// delete request comments
superagent
    .delete("http://localhost:3000/comments/1")
    .expect(204)
    .then(callback)
    .catch(errorCallback)

// delete request profile
superagent
    .delete("http://localhost:3000/profile/1")
    .expect(204)
    .then(callback)
    .catch(errorCallback)